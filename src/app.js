import showdown from 'showdown';
import hljs from 'highlight.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'highlight.js/styles/github.css';
import 'bootstrap/dist/js/bootstrap';

/**
 * @param variable
 * @returns {string}
 */
const getQueryVariable = (variable) => {
  const query = window.location.search.substring(1);
  const vars = query.split('&');
  for (let i = 0; i < vars.length; i += 1) {
    const pair = vars[i].split('=');
    if (decodeURIComponent(pair[0]) === variable) {
      return decodeURIComponent(pair[1]);
    }
  }
  return '';
};

/**
 * @param page
 * @returns {string}
 */
const renderMd = (page) => {
  let content = '';
  const converter = new showdown.Converter();
  try {
    const text = require(`../wiki/${page}.md`);
    content = converter.makeHtml(text);
  } catch (e) {
    content = 'not found';
  }
  return content;
};

document.getElementById('loading').style.visibility = 'hidden';
document.getElementById('content-row').style.visibility = 'visible';

const page = getQueryVariable('page');
const content = (page) ? renderMd(page) : renderMd('index');
document.getElementById('content').innerHTML = content;
document.querySelectorAll('code').forEach((block) => {
  hljs.highlightBlock(block);
});
